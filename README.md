# RealignJson

## What it do?

This program realigns a target json file using the object property ordering found in a specified template json file.

## Why?

This is especially useful when making alterations to source repository tracked json objects.

If the alterations are made by an automated tool, the ordering of the json properties may change depending upon the json serializer implementation, causing diffs of the old and new versions of the file to be messy even when only minor changes are made to the structure of the file.

Running this tool on the new version of the file using the old (or git HEAD) version as the template makes the diffs between the old and the new become much more readable.

## Where can I get it?

A single binary is available here:
https://bitbucket.org/bertkw/realignjson/downloads/

The binary was created in Visual Studio, with the various program dependencies packaged into the binary using Costura.Fody.

## Details
Example: say we have a json object
```json
{
    "a": 4,
    "b": 5,
    "c": 6,
    "d": 7
}
```
realigned using the template:
```json
{
    "c": 4,
    "b": 5,
    "a": 6
}
```
It will become:
```json
{
    "c": 6,
    "b": 5,
    "a": 4,
    "d": 7
}
```
Notice that the new property "d", which exists in the target object but not the template, is placed at the end, and the 3 matching properties are re-ordered using the order in the template.

This operation is repeated recursively on the target json's object tree.  If objects in the target json subtree are found that have no direct counterpart in the template json subtree, attempts are made to infer a proper property ordering from the set of properties on the unmatched target object, using a schema built from the entire template file.

Arrays are handled in a similar manner, but no effort is made to detect insertions within the middle of the array; when an array exists in the target with a counterpart in the template the elements of the arrays are simply matched up side by side.

## Usage
```
realignjson [--stdout] targetfilename templatefilename
```
The program's normal behaviour is to overwrite the target file with the modified json.

The "--stdout" flag will cause the program to dump its output to stdout.

```
realignjson --git [--stdout] targetfilename
```
The "--git" flag will cause the program to attempt to retrieve the HEAD revision of the target file as the template, instead of a specified file.

## Building

The program is buildable using both Visual Studio 2017 Community Edition and Visual Studio Code.  There is a separate project directory for each IDE.

The Visual Studio 2017 project packages the executable with its dependencies into a single binary; the Visual Studio Code version does not.

## License

RealignJson is under the MIT license.

## Credit

Most of the parser is copied and pasted directly from the source code of the Chiron library, which is also available under the MIT license.