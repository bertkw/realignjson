module Prelude
open System.IO
open System.Text
open System.Collections.Generic
open System

type ProcessResult = { ExitCode: int; StdOut : string; StdErr : string }
let exec exe args =
    let psi = new System.Diagnostics.ProcessStartInfo(exe, args)
    psi.UseShellExecute <- false
    psi.RedirectStandardOutput <- true
    psi.RedirectStandardError <- true
    psi.CreateNoWindow <- true
    use p = System.Diagnostics.Process.Start(psi)

    // let output = ResizeArray<_>()
    // let error = ResizeArray<_>()
    let output = new StringBuilder()
    let error = new StringBuilder()
    let createDataHandler (buffer: StringBuilder) =
        fun (data:System.Diagnostics.DataReceivedEventArgs) ->
            if not (isNull data.Data) then buffer.AppendLine(data.Data) |> ignore
    p.OutputDataReceived.Add (createDataHandler output)
    p.ErrorDataReceived.Add (createDataHandler error)
    p.BeginErrorReadLine()
    p.BeginOutputReadLine()
    p.WaitForExit()
    { ExitCode = p.ExitCode; StdOut = output.ToString(); StdErr = error.ToString() }

let exec_wd workingDir exe args =
    let psi = new System.Diagnostics.ProcessStartInfo(exe, args)
    psi.UseShellExecute <- false
    psi.RedirectStandardOutput <- true
    psi.RedirectStandardError <- true
    psi.CreateNoWindow <- true
    psi.WorkingDirectory <- workingDir
    use p = System.Diagnostics.Process.Start(psi)

    // let output = ResizeArray<_>()
    // let error = ResizeArray<_>()
    let output = new StringBuilder()
    let error = new StringBuilder()
    let createDataHandler (buffer: StringBuilder) =
        fun (data:System.Diagnostics.DataReceivedEventArgs) ->
            if not (isNull data.Data) then buffer.AppendLine(data.Data) |> ignore
    p.OutputDataReceived.Add (createDataHandler output)
    p.ErrorDataReceived.Add (createDataHandler error)
    p.BeginErrorReadLine()
    p.BeginOutputReadLine()
    p.WaitForExit()
    { ExitCode = p.ExitCode; StdOut = output.ToString(); StdErr = error.ToString() }

let rec list_files extension path =
    Seq.append
        (Directory.EnumerateFiles(path, extension))
        (path |> Directory.EnumerateDirectories |> Seq.collect (list_files extension))

let git_show_head filename =
    let wdPath =
        Path.GetDirectoryName(filename)
        |> fun s -> if String.IsNullOrEmpty s then "." else s
        |> Path.GetFullPath
    let fn = Path.GetFileName(filename)
    let pr = exec_wd wdPath "git" (sprintf "show HEAD:\"./%s\"" fn)
    pr.StdOut

let memoize f =
    let cache = Dictionary<_, _>()
    fun x ->
        if cache.ContainsKey(x) then cache.[x]
        else let res = f x
             cache.[x] <- res
             res
// git_show_head @"C:\repos\StatusPotentiae\StatusPotentiae\Settings.fsx"