module RealignJson

open System.IO
open Chiron.Ordered
open Prelude

let usage () =
    printfn "realigns target file according to the ordering used in templatefilename"
    printfn "Usage:"
    printfn "    realignjson [--stdout] targetfilename templatefilename"
    printfn "    realignjson --git [--stdout] targetfilename"

// let isGitMode argv = (argv:string array).[0] = "--git"

type NormalMode = { UseStdOut: bool; TargetFilename: string; TemplateFilename: string }
type GitMode = { UseStdOut: bool; TargetFilename: string }
type Mode =
| NormalMode of NormalMode
| GitMode of GitMode
| PrintUsage

let (|IsGitMode|_|) (argv:string array) =
    if (Array.length argv < 2) || argv.[0] <> "--git"
        then None
    elif argv.[1] = "--stdout" && argv.Length > 2 && File.Exists argv.[2] then
        { UseStdOut = true; TargetFilename = argv.[2] } |> Some
    elif File.Exists argv.[1] then
        { UseStdOut = false; TargetFilename = argv.[1] } |> Some
    else None
let (|IsNormalMode|_|) (argv:string array) =
    if argv.Length > 2 && argv.[0] = "--stdout" && File.Exists argv.[1] && File.Exists argv.[2] then
        { UseStdOut = true; TargetFilename = argv.[1]; TemplateFilename = argv.[2] } |> Some
    elif argv.Length > 1 && File.Exists argv.[0] && File.Exists argv.[1] then
        { UseStdOut = false; TargetFilename = argv.[0]; TemplateFilename = argv.[1] } |> Some
    else None

let getOpts argv =
    match argv with
    | IsNormalMode opts -> opts |> NormalMode
    | IsGitMode opts -> opts |> GitMode
    | _ -> PrintUsage

let do_work targetText templateText =
    let target = OrderedJson.parse targetText
    let template = OrderedJson.parse templateText
    let realigned = OrderedJson.reorder target template
    let formatOptions = OrderedJson.determine_formatting templateText
    OrderedJson.formatWith formatOptions realigned

let do_output text filename useStdOut =
    if useStdOut
    then printfn "%s" text
    else
        File.Delete filename
        File.WriteAllText(filename, text)

[<EntryPoint>]
let main argv =
    match getOpts argv with
    | PrintUsage ->
        usage()
        0
    | NormalMode nmOpts ->
        let targetText = File.ReadAllText nmOpts.TargetFilename
        let templateText = File.ReadAllText nmOpts.TemplateFilename
        let realignedText = do_work targetText templateText
        do_output realignedText nmOpts.TargetFilename nmOpts.UseStdOut
        0
    | GitMode gitOpts ->
        let targetText = File.ReadAllText gitOpts.TargetFilename
        let templateText = git_show_head gitOpts.TargetFilename
        let realignedText = do_work targetText templateText
        do_output realignedText gitOpts.TargetFilename gitOpts.UseStdOut
        0