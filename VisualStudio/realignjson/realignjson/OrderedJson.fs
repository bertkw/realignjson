namespace Chiron.Ordered
open System
open System.Globalization
open System.Text
open FParsec

type OrderedJson =
    | OjArray of OrderedJson list
    | OjBool of bool
    | OjNull of unit
    | OjNumber of decimal
    | OjObject of (Map<string, OrderedJson> * string list)
    | OjString of string

[<RequireQualifiedAccess>]
module internal Escaping =
    let inline private digit i =
            (i >= 0x30 && i <= 0x39)

    let inline private hexdig i =
            (digit i)
         || (i >= 0x41 && i <= 0x46)
         || (i >= 0x61 && i <= 0x66)

    let inline private unescaped i =
            i >= 0x20 && i <= 0x21
         || i >= 0x23 && i <= 0x5b
         || i >= 0x5d && i <= 0x10ffff

    let private unescapedP =
        satisfy (int >> unescaped)

    let private hexdig4P =
        manyMinMaxSatisfy 4 4 (int >> hexdig)
        |>> fun s ->
            char (Int32.Parse (s, NumberStyles.HexNumber))

    let private escapedP =
            skipChar '\\'
        >>. choice [
                pchar '"'
                pchar '\\'
                pchar '/'
                skipChar 'b' >>% '\u0008'
                skipChar 'f' >>% '\u000c'
                skipChar 'n' >>% '\u000a'
                skipChar 'r' >>% '\u000d'
                skipChar 't' >>% '\u0009'
                skipChar 'u' >>. hexdig4P ]

    let private charP =
        choice [
            unescapedP
            escapedP ]

    let parse =
        many charP

    let private escapeChars =
        [| '"'; '\\'; '\n'; '\r'; '\t'; '\b'; '\f'
           '\u0000'; '\u0001'; '\u0002'; '\u0003'
           '\u0004'; '\u0005'; '\u0006'; '\u0007'
           '\u000B'; '\u000E'; '\u000F'
           '\u0010'; '\u0011'; '\u0012'; '\u0013'
           '\u0014'; '\u0015'; '\u0016'; '\u0017'
           '\u0018'; '\u0019'; '\u001A'; '\u001B'
           '\u001C'; '\u001D'; '\u001E'; '\u001F' |]

    let escape (s: string) : string =
        let mutable nextEscapeIndex = s.IndexOfAny (escapeChars)
        if nextEscapeIndex = -1 then s else
        let sb = System.Text.StringBuilder (String.length s)
        let mutable lastIndex = 0
        while (nextEscapeIndex <> -1) do
            if nextEscapeIndex > lastIndex then
                sb.Append (s, lastIndex, nextEscapeIndex - lastIndex) |> ignore
            match s.[nextEscapeIndex] with
            | '"' -> sb.Append @"\"""
            | '\\' -> sb.Append @"\\"
            | '\n' -> sb.Append @"\n"
            | '\r' -> sb.Append @"\r"
            | '\t' -> sb.Append @"\t"
            | '\f' -> sb.Append @"\f"
            | '\b' -> sb.Append @"\b"
            | '\u0000' -> sb.Append @"\u0000"
            | '\u0001' -> sb.Append @"\u0001"
            | '\u0002' -> sb.Append @"\u0002"
            | '\u0003' -> sb.Append @"\u0003"
            | '\u0004' -> sb.Append @"\u0004"
            | '\u0005' -> sb.Append @"\u0005"
            | '\u0006' -> sb.Append @"\u0006"
            | '\u0007' -> sb.Append @"\u0007"
            | '\u000B' -> sb.Append @"\u000B"
            | '\u000E' -> sb.Append @"\u000E"
            | '\u000F' -> sb.Append @"\u000F"
            | '\u0010' -> sb.Append @"\u0010"
            | '\u0011' -> sb.Append @"\u0011"
            | '\u0012' -> sb.Append @"\u0012"
            | '\u0013' -> sb.Append @"\u0013"
            | '\u0014' -> sb.Append @"\u0014"
            | '\u0015' -> sb.Append @"\u0015"
            | '\u0016' -> sb.Append @"\u0016"
            | '\u0017' -> sb.Append @"\u0017"
            | '\u0018' -> sb.Append @"\u0018"
            | '\u0019' -> sb.Append @"\u0019"
            | '\u001A' -> sb.Append @"\u001A"
            | '\u001B' -> sb.Append @"\u001B"
            | '\u001C' -> sb.Append @"\u001C"
            | '\u001D' -> sb.Append @"\u001D"
            | '\u001E' -> sb.Append @"\u001E"
            | '\u001F' -> sb.Append @"\u001F"
            | c -> sb.Append(@"\u").Append((int c).ToString("X4"))
            |> ignore
            lastIndex <- nextEscapeIndex + 1
            nextEscapeIndex <- s.IndexOfAny (escapeChars, lastIndex)
        if lastIndex < String.length s then
            sb.Append (s, lastIndex, String.length s - lastIndex) |> ignore
        sb.ToString ()

(* Parsing
   Functions for parsing string JSON data to Json types, using
   FParsec.
   Functions parse and tryParse are effectively static,
   while import parses the provided string JSON and replaces the
   current state of a Json<'a> function. *)

[<AutoOpen>]
module internal Parsing =

    (* Helpers
       Utlility functions for working with intermediate states of
       parsers, minimizing boilerplate and unpleasant code. *)

    let private emp =
        function | Some x -> x
                 | _ -> ""

    (* Grammar
       Common grammatical elements forming parts of other parsers as
       as defined in RFC 1759. The elements are implemented slightly
       differently due to the design of parser combinators used, chiefly
       concerning whitespace, which is always implemented as trailing.
       Taken from RFC 7159, Section 2 Grammar
       See [http://tools.ietf.org/html/rfc7159#section-2] *)

    let private wsp i =
            i = 0x20
         || i = 0x09
         || i = 0x0a
         || i = 0x0d

    let private wspP =
        skipManySatisfy (int >> wsp)

    let private charWspP c =
        skipChar c .>> wspP

    let private beginArrayP =
        charWspP '['

    let private beginObjectP =
        charWspP '{'

    let private endArrayP =
        charWspP ']'

    let private endObjectP =
        charWspP '}'

    let private nameSeparatorP =
        charWspP ':'

    let private valueSeparatorP =
        charWspP ','

    (* JSON
       As the JSON grammar is recursive in various forms, we create a
       reference parser which will be assigned later, allowing for recursive
       definition of parsing rules. *)

    let private jsonP, jsonR =
        createParserForwardedToRef ()

    (* Values
       Taken from RFC 7159, Section 3 Values
       See [http://tools.ietf.org/html/rfc7159#section-3] *)

    let private boolP =
            stringReturn "true" true
        <|> stringReturn "false" false
        .>> wspP

    let private nullP =
        stringReturn "null" () .>> wspP

    (* Numbers
       The numbers parser is implemented by parsing the JSON number value
       in to a known representation valid as string under Double.Parse
       natively (invoked as the float conversion function on the eventual
       string).
       Taken from RFC 7159, Section 6 Numbers
       See [http://tools.ietf.org/html/rfc7159#section-6] *)

    let private digit1to9 i =
            i >= 0x31 && i <= 0x39

    let private digit i =
            digit1to9 i
         || i = 0x30

    let private e i =
            i = 0x45
         || i = 0x65

    let private minusP =
        charReturn '-' "-"

    let private intP =
        charReturn '0' "0" <|> (satisfy (int >> digit1to9) .>>. manySatisfy (int >> digit)
        |>> fun (h, t) -> string h + t)

    let private fracP =
        skipChar '.' >>.  many1Satisfy (int >> digit)
        |>> fun i -> "." + i

    let private expP =
            skipSatisfy (int >> e)
        >>. opt (charReturn '-' "-" <|> charReturn '+' "+")
        .>>. many1Satisfy (int >> digit)
        |>> function | Some s, d -> "e" + s + d
                     | _, d -> "e" + d

    let private numberP =
        pipe4 (opt minusP) intP (opt fracP) (opt expP) (fun m i f e ->
            decimal (emp m + i + emp f + emp e)) .>> wspP

    (* Strings
       Taken from RFC 7159, Section 7 Strings
       See [http://tools.ietf.org/html/rfc7159#section-7] *)

    let private quotationMarkP =
        skipChar '"'

    let private stringP =
        between quotationMarkP quotationMarkP Escaping.parse .>> wspP
        |>> fun cs -> new string (List.toArray cs)

    (* Objects
       Taken from RFC 7159, Section 4 Objects
       See [http://tools.ietf.org/html/rfc7159#section-4] *)

    let private memberP =
        stringP .>> nameSeparatorP .>>. jsonP

    let private objectP =
        between beginObjectP endObjectP (sepBy memberP valueSeparatorP)
        |>> fun m -> (Map.ofList m, List.map fst m)

    (* Arrays
       Taken from RFC 7159, Section 5 Arrays
       See [http://tools.ietf.org/html/rfc7159#section-5] *)

    let private arrayP =
        between beginArrayP endArrayP (sepBy jsonP valueSeparatorP)

    (* JSON *)

    do jsonR :=
            wspP
        >>. choice [
                arrayP  |>> OjArray
                boolP   |>> OjBool
                nullP   |>> OjNull
                numberP |>> OjNumber
                objectP |>> OjObject
                stringP |>> OjString ]

    let inline append (s: string) (b: StringBuilder) = b.Append s

    [<RequireQualifiedAccess>]
    module OrderedJson =
        type Json2<'a> = OrderedJson -> OrderedJsonResult<'a> * OrderedJson

        and OrderedJsonResult<'a> =
            | Value of 'a
            | Error of string

        let internal parseJson s =
            if String.IsNullOrWhiteSpace s then
                failwith "Input is null or whitespace"
            else
                match run jsonP s with
                | Success (json, _, _) -> Value json
                | Failure (e, _, _) -> Error e

        let tryParse =
               parseJson
            >> function | Value json -> Choice1Of2 json
                        | Error e -> Choice2Of2 e

        let parse =
               parseJson
            >> function | Value json -> json
                        | Error e -> failwith e

        let import s =
            fun json ->
                match parseJson s with
                | Value json -> Value (), json
                | Error e -> Error e, json

        open Chiron
        let rec toJson oj : Json =
            match oj with
            | OrderedJson.OjArray ojl -> Json.Array <| (List.map toJson ojl)
            | OrderedJson.OjBool b -> Json.Bool b
            | OrderedJson.OjNull () ->  Json.Null <| ()
            | OrderedJson.OjNumber d -> Json.Number d
            | OrderedJson.OjObject (m, _) -> Json.Object <| (Map.map (fun _ v -> toJson v) m)
            | OrderedJson.OjString s -> Json.String <| s
        let rec ofJson j =
            match j with
            | Json.Array ojl -> OrderedJson.OjArray <| (List.map ofJson ojl)
            | Json.Bool b -> OrderedJson.OjBool b
            | Json.Null () ->  OrderedJson.OjNull <| ()
            | Json.Number d -> OrderedJson.OjNumber d
            | Json.Object m ->
                let m2 = m |> Map.map (fun _ v -> ofJson v)
                let keys = m |> Map.toList |> List.map fst
                (m2, keys) |> OrderedJson.OjObject
            | Json.String s -> OrderedJson.OjString <| s

type OrderedJsonFormattingOptions =
  { Spacing : StringBuilder -> StringBuilder
    NewLine : int -> StringBuilder -> StringBuilder }

  static member Compact =
    { Spacing = id
      NewLine = fun _ x -> x }

  static member SingleLine =
    { Spacing = append " "
      NewLine = fun _ -> append " " }

  static member CustomSpacing spaces =
    let spaceString = String.replicate spaces " "
    { Spacing = append " "
      NewLine = fun level -> append "\n" >> append (String.replicate level spaceString) }

  static member Pretty = OrderedJsonFormattingOptions.CustomSpacing 2

module private Formatting =
    (* Helpers *)
    type private Formatter<'a> =
        'a -> StringBuilder -> StringBuilder

    type private Separator =
        StringBuilder -> StringBuilder


    let private appendf (s: string) (v1: obj) (b: StringBuilder) =
        b.AppendFormat (s, v1)

    let private join<'a> (f: Formatter<'a>) (s: Separator) =
        let rec join values (b: StringBuilder) =
            match values with
            | [] -> b
            | [v] -> f v b
            | v :: vs -> (f v >> s >> join vs) b

        join

    (* Formatters *)
    let rec formatJson level options =
        function | OjArray x -> formatArray level options x
                 | OjBool x -> formatBool x
                 | OjNumber x -> formatNumber x
                 | OjNull _ -> formatNull ()
                 | OjObject x -> formatObject level options x
                 | OjString x -> formatString x

    and private formatArray level options =
        function | x ->
                    if x.Length > 0 then
                           append "["
                        >> options.NewLine (level + 1)
                        >> join (formatJson (level + 1) options) (append "," >> options.NewLine (level + 1)) x
                        >> options.NewLine level
                        >> append "]"
                    else
                        append "[]"

    and private formatBool =
        function | true -> append "true"
                 | _ -> append "false"

    and private formatNumber =
        function | x -> append (string x)

    and private formatNull =
        function | () -> append "null"

    and private formatObject level options =
        function | (kvs, keys) ->
                    if kvs.Count = 0 then
                        append "{}"
                    else
                        append "{"
                        >> options.NewLine (level + 1)
                        >> join (fun (k, v) -> appendf "\"{0}\":" (Escaping.escape k) >> options.Spacing >> formatJson (level + 1) options v)
                                (append "," >> options.NewLine (level + 1))
                                (keys |> List.map (fun key -> (key, Map.find key kvs)))
                        >> options.NewLine level
                        >> append "}"
    and private formatString =
        function | x -> appendf "\"{0}\"" (Escaping.escape x)

[<RequireQualifiedAccess>]
module OrderedJson =
    let format json =
        StringBuilder ()
        |> Formatting.formatJson 0 OrderedJsonFormattingOptions.Compact json
        |> string

    let formatWith options json =
        StringBuilder ()
        |> Formatting.formatJson 0 options json
        |> string

    let rec private reorder_impl (find_best_order:string list -> string list) target templateOption =
        // Recursion helpers
        let inline reorder_some x y = reorder_impl find_best_order x (Some y)
        let inline reorder_none x = reorder_impl find_best_order x None

        // array handler
        let inline reorder_array targetArray =
            match templateOption with
            | Some (OrderedJson.OjArray templateArray) ->
                if (List.length targetArray) <= templateArray.Length then
                    (targetArray, (templateArray |> List.take (List.length targetArray)))
                    ||> List.map2 reorder_some
                    |> OrderedJson.OjArray
                else
                    let matchables =
                        ((targetArray |> List.take (List.length templateArray)), templateArray)
                        ||> List.map2 reorder_some
                    let unmatchables =
                        targetArray
                        |> List.skip (List.length templateArray)
                        |> List.map reorder_none
                    [ matchables; unmatchables ]
                    |> List.concat
                    |> OrderedJson.OjArray
            | Some (_)
            | None ->
                targetArray
                |> List.map reorder_none
                |> OrderedJson.OjArray

        // object handler
        let inline reorder_object (m, keyList) =
            match templateOption with
            | Some (OrderedJson.OjObject (templateMap, templateKeys)) ->
                // Match, so create an ordering based off of the templateKeys list
                let commonKeys = templateKeys |> List.filter (fun key -> Map.containsKey key m)
                let missingKeys = keyList |> List.filter (templateMap.ContainsKey >> not)
                let ordering = List.concat [ commonKeys; missingKeys ]
                let m2 =
                    m
                    // For each child, reorder as well, matching with the template version if possible
                    |> Map.map (fun k v ->
                        match Map.tryFind k templateMap with
                        | Some oj -> reorder_some v oj
                        | None -> reorder_none v
                    )
                (m2, ordering) |> OrderedJson.OjObject
            | Some (_)
            | None ->
                // No match, so try to find an ordering from template data
                let ordering = find_best_order keyList
                // Reorder the subobjects
                let m2 = m |> Map.map (fun _ v -> reorder_none v)
                (m2, ordering) |> OrderedJson.OjObject

        match target with
        | OrderedJson.OjArray l -> reorder_array l
        | OrderedJson.OjBool b -> OrderedJson.OjBool b
        | OrderedJson.OjNull _ ->  OrderedJson.OjNull ()
        | OrderedJson.OjNumber d -> OrderedJson.OjNumber d
        | OrderedJson.OjObject o -> reorder_object o
        | OrderedJson.OjString s -> OrderedJson.OjString s

    let reorder target template =
        // recursively build a list of (keySet, orderedKeys) data
        let rec collect_patterns oj =
            match oj with
            | OrderedJson.OjObject (m, orderedKeys) ->
                let keySet = Set.ofList orderedKeys
                let childData =
                    m
                    |> Map.toSeq
                    |> (Seq.collect (fun (_,v) -> collect_patterns v))
                seq { yield! childData; yield (keySet, orderedKeys)}
            | OrderedJson.OjArray l -> Seq.collect collect_patterns l
            | _ -> Seq.empty

        // returns a function that finds the best ordering given an ordering as a string list,
        // using only the object orderings found in the template file
        let build_ordering_function template =
            // Array of (keySets, (ordering, occurrenceCount)) tuples
            let keySetToOrderingArray =
                template
                |> collect_patterns // grab (keySet, ordering) for every object in the json object
                |> Seq.groupBy fst
                |> Seq.map (fun (keySet, keyGroups) ->
                    // for each keySet, find the key ordering that occurs
                    // the most frequently and discard the rest
                    let bestMatch =
                        keyGroups
                        |> Seq.map snd
                        |> Seq.countBy id
                        |> Seq.maxBy snd
                    keySet, bestMatch
                )
                |> Seq.toArray

            let uniqueKeySetsSortedDesc =
                keySetToOrderingArray
                // Group by set size
                |> Array.groupBy (fun (set, _) -> Set.count set)
                // Sort subArrays by their relative occurrences, to prefer the keySets that occur more frequently
                |> Array.map (fun (count,subArray) -> count, subArray |> Array.sortByDescending (fun (_,i) -> i))
                // Sort by the set key counts
                |> Array.sortByDescending fst
                // Strip out extra info
                |> Array.collect snd
                |> Array.map fst

            let keySetToOrderingMap =
                keySetToOrderingArray
                |> Array.map (fun (set,(ordering,_)) -> (set,ordering))
                |> Map.ofArray

            let inline findFromUniqueKeys f = uniqueKeySetsSortedDesc |> Array.tryFind f
            let find_best (keyList:string list) =
                let keySet = Set.ofList keyList
                match Map.tryFind keySet keySetToOrderingMap with
                | Some ordering -> ordering  // Found complete match, yay!
                | None ->
                    findFromUniqueKeys (Set.isSubset keySet) // Find the largest keyset of which our current is a subset
                    |> function
                        | Some templateSet -> Some templateSet
                        | None -> findFromUniqueKeys (Set.isSuperset keySet) // Find the largest subset of our current keySet
                    |> function
                    | Some templateSet ->
                        let templateKeys = Map.find templateSet keySetToOrderingMap
                        let commonKeys = templateKeys |> List.filter (fun key -> Set.contains key keySet)
                        let missingKeys = keyList |> List.filter (fun key -> Set.contains key templateSet |> not)
                        List.concat [ commonKeys; missingKeys ]
                    | None -> keyList // If absolutely nothing is found, just use the current order

            Prelude.memoize find_best // safe to memoize

        let find_best_order = build_ordering_function template
        reorder_impl find_best_order target (Some template)

    let determine_formatting (rawJson:string) =
        let countLeadingSpaces s = s |> Seq.takeWhile (fun c -> c = ' ') |> Seq.length
        let lines = rawJson.Trim().Split([| '\n' |], 64)
        match lines.Length with
        | 0
        | 1 -> OrderedJsonFormattingOptions.SingleLine
        | 2 -> OrderedJsonFormattingOptions.Pretty
        | _ ->
            let x =
                lines
                |> Array.pairwise
                |> Array.map (fun (l1, l2) -> (countLeadingSpaces l1) - (countLeadingSpaces l2) |> Math.Abs)
                |> Array.filter (fun x -> x > 0)
                |> Array.countBy id
                |> Array.maxBy snd
                |> fst
            x
            |> OrderedJsonFormattingOptions.CustomSpacing